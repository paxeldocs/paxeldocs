---
title: Getting Started
sidebarDepth: 2
---

<div class="reset-mobile-brand">
<img width="100%" src="/assets/korlap/header.png">
</div>

## Introduction
**Korlap App** is internally use mobile Application that only intended for **Korlap** 

## Process

`Add New Feature / Fixing Bug` > `Dev QA` > `Stagging QA` > `Production QA` > `Release`

## Environment
Each **flavor** has different variables that match for each build, you need to **change** the variables depend on the requirements

- `production`
- `staging`
- `dev`

```js
// file: app/build.gradle
production {
            versionNameSuffix ""
            manifestPlaceholders = [googleMapsKey: GOOGLE_MAP_KEY_PROD]
           }
           ...

// file: repository/build.gradle
production {
            buildConfigField "String", "API_URL", '"https://example.paxel.co/cig/api/v1/"'
            buildConfigField "String", "APK_URL", '"https://drive.google.com/uc?authuser=0&id=example"'
           }
           ...
```
***

## Repository
### Bitbucket
```text
git clone git@bitbucket.org:paxelit/korlapapp-2.0.git
```

### Branch
Each branch represents development process. For example if you have assigned to add new feature you have to push your code change under **new branch** `feature/newFeatureBranch`, then if you want to build for **Dev QA** you have to **merge** your working branch to `dev` and so on. And finale if your **Prod QA** process has pass you can merge your code to 'master' branch wich is the production ready branch.

- `feature`
- `hotfix`
- `dev`
- `staging`
- `master`

### Commit Message
- `feat` Add new features
- `fix` Fix the problem/BUG
- `perf` Optimization/performance improvement
- `refactor` Refactor
- `revert` Undo edit
- `test` Test related
- `docs` Documentation/notes
- `chore` Library update/scaffolding configuration modification etc.
- `workflow` Workflow improvements

***
