---
title: Modules
sidebarDepth: 2
---
Modules are collection of source files and build settings that allow you to divide a project into discrete units of functionality.

<img src="/assets/korlap/architecture.png" width="300" align="right" hspace="20">

- `:app`
- `:bluetooth`
- `:repository`

## App module
The `:app` module is an com.android.application. Contain UI and View Model files.

## Bluetooth module
The `:bluetooth` module contains function and library to communicate and print to a bluetooth Printer.

## Repository module
The `:repository` module is a data repository and network service. For example an interface to call an API with GET method.
```kotlin
package com.paxelkorlapmobile.network

interface Api {
    @GET("settings")
    suspend fun getSettings(): BaseResponse<String, Settings>
 }
```
***
