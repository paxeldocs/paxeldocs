module.exports = [
	{
		title: "Hero App",
		collapsable: true,
		children:[
			"/mobile/hero/requirement",
			"/mobile/hero/modules",
			"/mobile/hero/dependencies",
		]
	},
	{
		title: "Korlap App",
		collapsable: true,
		children:[
			"/mobile/korlap/requirement",
			"/mobile/korlap/modules",
			"/mobile/korlap/dependencies",
		]
	},
];
