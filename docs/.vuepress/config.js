module.exports = {
  title: "Paxel Docs",
  theme: "antdocs",
  description: "a Documentations for Paxelian",
  base: "/",
  dest: 'dist',
  head: [
    ["link", { rel: "icon", href: "/assets/icon.png" }],
    ["script", { type: "text/javascript", src: "/assets/js/push.js" }],
    ['meta', { name: 'theme-color', content: '#5E50A1' }],
    ["meta", { name: "referrer", content: "never" }],
    ["meta",
      {
        name: "keywords",
        content:
          "paxel, paxel docs, documentation, doc, docs, korlap, hero"
      }
    ],
    ['meta', { property: 'og:type', content: 'website' }],
    ['meta', { property: 'og:title', content: 'Paxel Documentation' }],
    ['meta', { property: 'og:site_name', content: 'Paxel' }],
  ],
  plugins: [
    ["vuepress-plugin-code-copy", {
      selector: 'Copy',
      color: "#5E50A1",
      backgroundTransition: true,
      successText: 'Copied'
    }
    ],
    [
      'sitemap', {
          hostname: 'https://paxel-docs.netlify.app'
    },
    ],
  ],
  markdown: {
    lineNumbers: false,
    anchor: {
      permalinkBefore: false
    }
  },
  themeConfig: {
    backToTop: true,
    smoothScroll: true,
    logo: "/assets/icon_only.png",
    nav: require("./config/nav"),
    sidebar: require("./config/sidebar"),
    sidebarDepth: 0,
    lastUpdated: true,
    //repo: "https://github.com/zpfz/vuepress-theme-antdocs",
    editLinks: false
  }
  // configureWebpack: (config, isServer) => {
  //   // config.entry = glob.sync("./dist/*.js");
  //   if (!isServer) {
  //     config.output = {
  //       libraryTarget: "commonjs2",
  //       path: path.resolve(__dirname, "dist"),
  //       filename: "bundle.js"
  //     };
  //   }
  // }
};
