export const STORAGE_KEY = 'paxel-docs-auth'

// Do user authorization verify
export function checkAuth () {
    const auth = JSON.parse(localStorage.getItem(STORAGE_KEY))
    if(!auth){
        return false
    }
    const timeLogin = auth.time
    const now = new Date().getTime()

    const diff = now - timeLogin
    const diffMinutes = diff / (60 * 1000);
    if(Math.floor(diffMinutes) >= 60){
       return false
    }
    return auth && Object.keys(auth).length
}
