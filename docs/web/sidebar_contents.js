module.exports = [
	'/web/',
	'getting-started',
	'changelog',
	'using-vuepress-creator',
	'writing-norms',
	'palette',
	'extend-config',
	'using-antd',
	'faq',
	'sponsor'
];
